# ---------------------------------------------------------------------------------------#
# ----------------------------------------------------------------------------- HEADER --#

"""
:author:
    LazyDodo, Jared Webber
    

:synopsis:
    

:description:
    

:applications:
    
:see_also:
   
:license:
    see license.txt and EULA.txt 

"""

bl_info = {
    "name": "OSLPY",
    "author": "LazyDodo, Jared Webber",
    "version": (0, 0, 2),
    "blender": (2, 79, 0),
    "location": "",
    "description": "Python implementation of OSL for Cycles/Blender",
    "category": "Node",
    "tracker_url": "",
    "wiki_url": ""
}

# ---------------------------------------------------------------------------------------#
# ---------------------------------------------------------------------------- IMPORTS --#

# from . import conf
# from . import addon_updater_ops
import nodeitems_utils
from nodeitems_utils import NodeCategory, NodeItem

from .core.osl_base.osl_py_addin import ShaderNodeOSLPY
from .utils.io import IO
# Check for Blender
try:
    import bpy
except ImportError:
    pass

try:
    from .setup import developer_utils
except:
    pass
if "developer_utils" not in globals():
    message = ("\n\n"
               "OSLPY cannot be registered correctly\n"
               "Troubleshooting:\n"
               "  1. Try installing the addon in the newest official Blender version.\n"
               "  2. Try installing the newest OSLPY version from Gitlab.\n")
    raise Exception(message)

# Import and Reload Submodules
import importlib
from . import developer_utils

importlib.reload(developer_utils)
modules = developer_utils.setup_addon_modules(__path__, __name__, "bpy" in locals())


# ---------------------------------------------------------------------------------------#
# -------------------------------------------------------------------------- FUNCTIONS --#


# ---------------------------------------------------------------------------------------#
# ---------------------------------------------------------------------------- CLASSES --#

#TODO: Move this into the proper package/module configuration
class ExtraNodesCategory(NodeCategory):
    @classmethod
    def poll(cls, context):
        return (context.space_data.tree_type == 'ShaderNodeTree' and
                context.scene.render.use_shading_nodes)

node_categories = [
    ExtraNodesCategory("SH_OSL_PY", "OslPY", items=[
        NodeItem("ShaderNodeOSLPY"),
        ]),
    ]


# ---------------------------------------------------------------------------------------#
# -------------------------------------------------------------------------- REGISTER ---#


def register_addon():
    """
    Blender specific registration function
    :return:
    """
    # Register Blender Properties and Handlers

    try:
        import pip
    except ImportError:
        IO.info("pip python package not found. Installing.")
        try:
            import ensurepip
            ensurepip.bootstrap(upgrade=True, default_pip=True)
        except ImportError:
            IO.info("pip cannot be configured or installed. ")


def unregister_addon():
    """
    Blender specific un-register function
    :return:
    """
    IO.info("Unregistered OSLPY")


def register():
    """
    Blender specific registration function
    :return: 
    """
    register_addon()
    bpy.utils.register_module(__name__)
    for module in modules:
        if hasattr(module, "register"):
            module.register()
            # Register Blender Properties and Handlers
            # addon_updater_ops.register_updater(bl_info)
            # conf.register()

    bpy.utils.register_class(ShaderNodeOSLPY)
    nodeitems_utils.register_node_categories("SH_OSL_PY", node_categories)


def unregister():
    """
    Blender specific un-register function
    :return: 
    """
    bpy.utils.unregister_module(__name__)
    for module in modules:
        if hasattr(module, "unregister"):
            module.unregister()
    # Unregister Blender Properties and Handlers
    # conf.unregister()
    # addon_updater_ops.unregister_updater()
    unregister_addon()

    nodeitems_utils.unregister_node_categories("SH_OSL_PY")
    bpy.utils.unregister_class(ShaderNodeOSLPY)

# if __name__ == "__main__":
#     register()

